﻿
namespace FaceIDApp
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gunaDragControl1 = new Guna.UI.WinForms.GunaDragControl(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.gunaElipse1 = new Guna.UI.WinForms.GunaElipse(this.components);
            this.BtnManejar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.BtnBuscar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.BtnRegistro = new Bunifu.Framework.UI.BunifuFlatButton();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnCerrar = new System.Windows.Forms.PictureBox();
            this.BtnMin = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.BtnCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnMin)).BeginInit();
            this.SuspendLayout();
            // 
            // gunaDragControl1
            // 
            this.gunaDragControl1.TargetControl = this.panel1;
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(435, 60);
            this.panel1.TabIndex = 6;
            // 
            // gunaElipse1
            // 
            this.gunaElipse1.TargetControl = this;
            // 
            // BtnManejar
            // 
            this.BtnManejar.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.BtnManejar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.BtnManejar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnManejar.BorderRadius = 0;
            this.BtnManejar.ButtonText = "Actualizar Datos";
            this.BtnManejar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnManejar.DisabledColor = System.Drawing.Color.Gray;
            this.BtnManejar.Iconcolor = System.Drawing.Color.Transparent;
            this.BtnManejar.Iconimage = global::FaceIDApp.Properties.Resources.pen_solid;
            this.BtnManejar.Iconimage_right = null;
            this.BtnManejar.Iconimage_right_Selected = null;
            this.BtnManejar.Iconimage_Selected = null;
            this.BtnManejar.IconMarginLeft = 0;
            this.BtnManejar.IconMarginRight = 0;
            this.BtnManejar.IconRightVisible = true;
            this.BtnManejar.IconRightZoom = 0D;
            this.BtnManejar.IconVisible = true;
            this.BtnManejar.IconZoom = 90D;
            this.BtnManejar.IsTab = false;
            this.BtnManejar.Location = new System.Drawing.Point(11, 149);
            this.BtnManejar.Name = "BtnManejar";
            this.BtnManejar.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.BtnManejar.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.BtnManejar.OnHoverTextColor = System.Drawing.Color.White;
            this.BtnManejar.selected = false;
            this.BtnManejar.Size = new System.Drawing.Size(411, 68);
            this.BtnManejar.TabIndex = 2;
            this.BtnManejar.Text = "Actualizar Datos";
            this.BtnManejar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnManejar.Textcolor = System.Drawing.Color.White;
            this.BtnManejar.TextFont = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnManejar.Click += new System.EventHandler(this.BtnManejar_Click);
            // 
            // BtnBuscar
            // 
            this.BtnBuscar.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.BtnBuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.BtnBuscar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnBuscar.BorderRadius = 0;
            this.BtnBuscar.ButtonText = "Buscar";
            this.BtnBuscar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnBuscar.DisabledColor = System.Drawing.Color.Gray;
            this.BtnBuscar.Iconcolor = System.Drawing.Color.Transparent;
            this.BtnBuscar.Iconimage = global::FaceIDApp.Properties.Resources.search_solid_copia;
            this.BtnBuscar.Iconimage_right = null;
            this.BtnBuscar.Iconimage_right_Selected = null;
            this.BtnBuscar.Iconimage_Selected = null;
            this.BtnBuscar.IconMarginLeft = 0;
            this.BtnBuscar.IconMarginRight = 0;
            this.BtnBuscar.IconRightVisible = true;
            this.BtnBuscar.IconRightZoom = 0D;
            this.BtnBuscar.IconVisible = true;
            this.BtnBuscar.IconZoom = 90D;
            this.BtnBuscar.IsTab = false;
            this.BtnBuscar.Location = new System.Drawing.Point(11, 245);
            this.BtnBuscar.Name = "BtnBuscar";
            this.BtnBuscar.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.BtnBuscar.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.BtnBuscar.OnHoverTextColor = System.Drawing.Color.White;
            this.BtnBuscar.selected = false;
            this.BtnBuscar.Size = new System.Drawing.Size(411, 68);
            this.BtnBuscar.TabIndex = 1;
            this.BtnBuscar.Text = "Buscar";
            this.BtnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnBuscar.Textcolor = System.Drawing.Color.White;
            this.BtnBuscar.TextFont = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBuscar.Click += new System.EventHandler(this.BtnBuscar_Click);
            // 
            // BtnRegistro
            // 
            this.BtnRegistro.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.BtnRegistro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.BtnRegistro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnRegistro.BorderRadius = 0;
            this.BtnRegistro.ButtonText = "Registro";
            this.BtnRegistro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnRegistro.DisabledColor = System.Drawing.Color.Gray;
            this.BtnRegistro.Iconcolor = System.Drawing.Color.Transparent;
            this.BtnRegistro.Iconimage = global::FaceIDApp.Properties.Resources.edit_solid;
            this.BtnRegistro.Iconimage_right = null;
            this.BtnRegistro.Iconimage_right_Selected = null;
            this.BtnRegistro.Iconimage_Selected = null;
            this.BtnRegistro.IconMarginLeft = 0;
            this.BtnRegistro.IconMarginRight = 0;
            this.BtnRegistro.IconRightVisible = true;
            this.BtnRegistro.IconRightZoom = 0D;
            this.BtnRegistro.IconVisible = true;
            this.BtnRegistro.IconZoom = 90D;
            this.BtnRegistro.IsTab = false;
            this.BtnRegistro.Location = new System.Drawing.Point(11, 341);
            this.BtnRegistro.Name = "BtnRegistro";
            this.BtnRegistro.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.BtnRegistro.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.BtnRegistro.OnHoverTextColor = System.Drawing.Color.White;
            this.BtnRegistro.selected = false;
            this.BtnRegistro.Size = new System.Drawing.Size(411, 68);
            this.BtnRegistro.TabIndex = 0;
            this.BtnRegistro.Text = "Registro";
            this.BtnRegistro.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnRegistro.Textcolor = System.Drawing.Color.White;
            this.BtnRegistro.TextFont = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRegistro.Click += new System.EventHandler(this.BtnRegistro_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(138, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 56);
            this.label1.TabIndex = 3;
            this.label1.Text = "Menú";
            // 
            // BtnCerrar
            // 
            this.BtnCerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnCerrar.Image = global::FaceIDApp.Properties.Resources.times_solid;
            this.BtnCerrar.Location = new System.Drawing.Point(386, 12);
            this.BtnCerrar.Name = "BtnCerrar";
            this.BtnCerrar.Size = new System.Drawing.Size(30, 30);
            this.BtnCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BtnCerrar.TabIndex = 4;
            this.BtnCerrar.TabStop = false;
            this.BtnCerrar.Click += new System.EventHandler(this.BtnCerrar_Click);
            // 
            // BtnMin
            // 
            this.BtnMin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnMin.Image = global::FaceIDApp.Properties.Resources.minus_solid;
            this.BtnMin.Location = new System.Drawing.Point(350, 12);
            this.BtnMin.Name = "BtnMin";
            this.BtnMin.Size = new System.Drawing.Size(30, 30);
            this.BtnMin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BtnMin.TabIndex = 5;
            this.BtnMin.TabStop = false;
            this.BtnMin.Click += new System.EventHandler(this.BtnMin_Click);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(24)))), ((int)(((byte)(57)))));
            this.ClientSize = new System.Drawing.Size(435, 442);
            this.Controls.Add(this.BtnMin);
            this.Controls.Add(this.BtnCerrar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnManejar);
            this.Controls.Add(this.BtnBuscar);
            this.Controls.Add(this.BtnRegistro);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            ((System.ComponentModel.ISupportInitialize)(this.BtnCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnMin)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI.WinForms.GunaDragControl gunaDragControl1;
        private Guna.UI.WinForms.GunaElipse gunaElipse1;
        private Bunifu.Framework.UI.BunifuFlatButton BtnManejar;
        private Bunifu.Framework.UI.BunifuFlatButton BtnBuscar;
        private Bunifu.Framework.UI.BunifuFlatButton BtnRegistro;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox BtnMin;
        private System.Windows.Forms.PictureBox BtnCerrar;
        private System.Windows.Forms.Panel panel1;
    }
}
﻿
namespace FaceIDApp
{
    partial class Update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Update));
            this.gunaElipse1 = new Guna.UI.WinForms.GunaElipse(this.components);
            this.Header = new System.Windows.Forms.Panel();
            this.BtnMin = new System.Windows.Forms.PictureBox();
            this.AppName = new System.Windows.Forms.Label();
            this.BtnClose = new System.Windows.Forms.PictureBox();
            this.gunaDragControl1 = new Guna.UI.WinForms.GunaDragControl(this.components);
            this.TxtApt = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.TxtEdif = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.BirthDate = new Guna.UI.WinForms.GunaDateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtCorreo = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.TxtCedula = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.btnSave = new Bunifu.Framework.UI.BunifuThinButton2();
            this.TxtName = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BtnMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnClose)).BeginInit();
            this.SuspendLayout();
            // 
            // gunaElipse1
            // 
            this.gunaElipse1.TargetControl = this;
            // 
            // Header
            // 
            this.Header.BackColor = System.Drawing.Color.Black;
            this.Header.Controls.Add(this.BtnMin);
            this.Header.Controls.Add(this.AppName);
            this.Header.Controls.Add(this.BtnClose);
            this.Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.Header.Location = new System.Drawing.Point(0, 0);
            this.Header.Name = "Header";
            this.Header.Size = new System.Drawing.Size(349, 41);
            this.Header.TabIndex = 3;
            // 
            // BtnMin
            // 
            this.BtnMin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnMin.Image = global::FaceIDApp.Properties.Resources.minus_solid;
            this.BtnMin.Location = new System.Drawing.Point(271, 10);
            this.BtnMin.Name = "BtnMin";
            this.BtnMin.Size = new System.Drawing.Size(31, 28);
            this.BtnMin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BtnMin.TabIndex = 2;
            this.BtnMin.TabStop = false;
            this.BtnMin.Click += new System.EventHandler(this.BtnMin_Click);
            // 
            // AppName
            // 
            this.AppName.AutoSize = true;
            this.AppName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AppName.ForeColor = System.Drawing.Color.White;
            this.AppName.Location = new System.Drawing.Point(30, 10);
            this.AppName.Name = "AppName";
            this.AppName.Size = new System.Drawing.Size(87, 19);
            this.AppName.TabIndex = 1;
            this.AppName.Text = "Actualizar";
            // 
            // BtnClose
            // 
            this.BtnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnClose.Image = global::FaceIDApp.Properties.Resources.times_solid;
            this.BtnClose.Location = new System.Drawing.Point(311, 9);
            this.BtnClose.Name = "BtnClose";
            this.BtnClose.Size = new System.Drawing.Size(26, 28);
            this.BtnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BtnClose.TabIndex = 0;
            this.BtnClose.TabStop = false;
            this.BtnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // gunaDragControl1
            // 
            this.gunaDragControl1.TargetControl = this.Header;
            // 
            // TxtApt
            // 
            this.TxtApt.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtApt.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.TxtApt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.TxtApt.HintForeColor = System.Drawing.Color.Empty;
            this.TxtApt.HintText = "";
            this.TxtApt.isPassword = false;
            this.TxtApt.LineFocusedColor = System.Drawing.Color.SeaGreen;
            this.TxtApt.LineIdleColor = System.Drawing.Color.Gray;
            this.TxtApt.LineMouseHoverColor = System.Drawing.Color.SeaGreen;
            this.TxtApt.LineThickness = 3;
            this.TxtApt.Location = new System.Drawing.Point(53, 259);
            this.TxtApt.Margin = new System.Windows.Forms.Padding(4);
            this.TxtApt.Name = "TxtApt";
            this.TxtApt.Size = new System.Drawing.Size(249, 33);
            this.TxtApt.TabIndex = 23;
            this.TxtApt.Text = "Apartamento";
            this.TxtApt.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.TxtApt.Enter += new System.EventHandler(this.TxtApt_Enter);
            this.TxtApt.Leave += new System.EventHandler(this.TxtApt_Leave);
            // 
            // TxtEdif
            // 
            this.TxtEdif.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtEdif.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.TxtEdif.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.TxtEdif.HintForeColor = System.Drawing.Color.Empty;
            this.TxtEdif.HintText = "";
            this.TxtEdif.isPassword = false;
            this.TxtEdif.LineFocusedColor = System.Drawing.Color.SeaGreen;
            this.TxtEdif.LineIdleColor = System.Drawing.Color.Gray;
            this.TxtEdif.LineMouseHoverColor = System.Drawing.Color.SeaGreen;
            this.TxtEdif.LineThickness = 3;
            this.TxtEdif.Location = new System.Drawing.Point(53, 218);
            this.TxtEdif.Margin = new System.Windows.Forms.Padding(4);
            this.TxtEdif.Name = "TxtEdif";
            this.TxtEdif.Size = new System.Drawing.Size(249, 33);
            this.TxtEdif.TabIndex = 22;
            this.TxtEdif.Text = "Edificio";
            this.TxtEdif.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.TxtEdif.Enter += new System.EventHandler(this.TxtEdif_Enter);
            this.TxtEdif.Leave += new System.EventHandler(this.TxtEdif_Leave);
            // 
            // BirthDate
            // 
            this.BirthDate.BaseColor = System.Drawing.Color.White;
            this.BirthDate.BorderColor = System.Drawing.Color.Silver;
            this.BirthDate.CustomFormat = null;
            this.BirthDate.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.BirthDate.FocusedColor = System.Drawing.Color.SeaGreen;
            this.BirthDate.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.BirthDate.ForeColor = System.Drawing.Color.Black;
            this.BirthDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.BirthDate.Location = new System.Drawing.Point(53, 299);
            this.BirthDate.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.BirthDate.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.BirthDate.Name = "BirthDate";
            this.BirthDate.OnHoverBaseColor = System.Drawing.Color.White;
            this.BirthDate.OnHoverBorderColor = System.Drawing.Color.SeaGreen;
            this.BirthDate.OnHoverForeColor = System.Drawing.Color.SeaGreen;
            this.BirthDate.OnPressedColor = System.Drawing.Color.Black;
            this.BirthDate.Size = new System.Drawing.Size(249, 30);
            this.BirthDate.TabIndex = 21;
            this.BirthDate.Text = "4/28/2021";
            this.BirthDate.Value = new System.DateTime(2021, 4, 28, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(109, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 19);
            this.label1.TabIndex = 20;
            this.label1.Text = "Datos Personales";
            // 
            // TxtCorreo
            // 
            this.TxtCorreo.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtCorreo.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.TxtCorreo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.TxtCorreo.HintForeColor = System.Drawing.Color.Empty;
            this.TxtCorreo.HintText = "";
            this.TxtCorreo.isPassword = false;
            this.TxtCorreo.LineFocusedColor = System.Drawing.Color.SeaGreen;
            this.TxtCorreo.LineIdleColor = System.Drawing.Color.Gray;
            this.TxtCorreo.LineMouseHoverColor = System.Drawing.Color.SeaGreen;
            this.TxtCorreo.LineThickness = 3;
            this.TxtCorreo.Location = new System.Drawing.Point(53, 177);
            this.TxtCorreo.Margin = new System.Windows.Forms.Padding(4);
            this.TxtCorreo.Name = "TxtCorreo";
            this.TxtCorreo.Size = new System.Drawing.Size(249, 33);
            this.TxtCorreo.TabIndex = 19;
            this.TxtCorreo.Text = "Correo";
            this.TxtCorreo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.TxtCorreo.Enter += new System.EventHandler(this.TxtCorreo_Enter);
            this.TxtCorreo.Leave += new System.EventHandler(this.TxtCorreo_Leave);
            // 
            // TxtCedula
            // 
            this.TxtCedula.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtCedula.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.TxtCedula.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.TxtCedula.HintForeColor = System.Drawing.Color.Empty;
            this.TxtCedula.HintText = "";
            this.TxtCedula.isPassword = false;
            this.TxtCedula.LineFocusedColor = System.Drawing.Color.SeaGreen;
            this.TxtCedula.LineIdleColor = System.Drawing.Color.Gray;
            this.TxtCedula.LineMouseHoverColor = System.Drawing.Color.SeaGreen;
            this.TxtCedula.LineThickness = 3;
            this.TxtCedula.Location = new System.Drawing.Point(53, 136);
            this.TxtCedula.Margin = new System.Windows.Forms.Padding(4);
            this.TxtCedula.Name = "TxtCedula";
            this.TxtCedula.Size = new System.Drawing.Size(249, 33);
            this.TxtCedula.TabIndex = 18;
            this.TxtCedula.Text = "Cedula";
            this.TxtCedula.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.TxtCedula.Enter += new System.EventHandler(this.TxtCedula_Enter);
            this.TxtCedula.Leave += new System.EventHandler(this.TxtCedula_Leave);
            // 
            // btnSave
            // 
            this.btnSave.ActiveBorderThickness = 1;
            this.btnSave.ActiveCornerRadius = 20;
            this.btnSave.ActiveFillColor = System.Drawing.Color.SeaGreen;
            this.btnSave.ActiveForecolor = System.Drawing.Color.White;
            this.btnSave.ActiveLineColor = System.Drawing.Color.SeaGreen;
            this.btnSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnSave.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSave.BackgroundImage")));
            this.btnSave.ButtonText = "Actualizar";
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.SeaGreen;
            this.btnSave.IdleBorderThickness = 1;
            this.btnSave.IdleCornerRadius = 20;
            this.btnSave.IdleFillColor = System.Drawing.Color.White;
            this.btnSave.IdleForecolor = System.Drawing.Color.SeaGreen;
            this.btnSave.IdleLineColor = System.Drawing.Color.SeaGreen;
            this.btnSave.Location = new System.Drawing.Point(87, 338);
            this.btnSave.Margin = new System.Windows.Forms.Padding(5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(181, 41);
            this.btnSave.TabIndex = 17;
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // TxtName
            // 
            this.TxtName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtName.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.TxtName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.TxtName.HintForeColor = System.Drawing.Color.Empty;
            this.TxtName.HintText = "";
            this.TxtName.isPassword = false;
            this.TxtName.LineFocusedColor = System.Drawing.Color.SeaGreen;
            this.TxtName.LineIdleColor = System.Drawing.Color.Gray;
            this.TxtName.LineMouseHoverColor = System.Drawing.Color.SeaGreen;
            this.TxtName.LineThickness = 3;
            this.TxtName.Location = new System.Drawing.Point(53, 95);
            this.TxtName.Margin = new System.Windows.Forms.Padding(4);
            this.TxtName.Name = "TxtName";
            this.TxtName.Size = new System.Drawing.Size(249, 33);
            this.TxtName.TabIndex = 16;
            this.TxtName.Text = "Nombre";
            this.TxtName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.TxtName.Enter += new System.EventHandler(this.TxtName_Enter);
            this.TxtName.Leave += new System.EventHandler(this.TxtName_Leave);
            // 
            // Update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(349, 409);
            this.Controls.Add(this.TxtApt);
            this.Controls.Add(this.TxtEdif);
            this.Controls.Add(this.BirthDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtCorreo);
            this.Controls.Add(this.TxtCedula);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.TxtName);
            this.Controls.Add(this.Header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Update";
            this.Text = "Update";
            this.Header.ResumeLayout(false);
            this.Header.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BtnMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnClose)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI.WinForms.GunaElipse gunaElipse1;
        private System.Windows.Forms.Panel Header;
        private System.Windows.Forms.PictureBox BtnMin;
        private System.Windows.Forms.Label AppName;
        private System.Windows.Forms.PictureBox BtnClose;
        private Guna.UI.WinForms.GunaDragControl gunaDragControl1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox TxtApt;
        private Bunifu.Framework.UI.BunifuMaterialTextbox TxtEdif;
        private Guna.UI.WinForms.GunaDateTimePicker BirthDate;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox TxtCorreo;
        private Bunifu.Framework.UI.BunifuMaterialTextbox TxtCedula;
        private Bunifu.Framework.UI.BunifuThinButton2 btnSave;
        private Bunifu.Framework.UI.BunifuMaterialTextbox TxtName;
    }
}
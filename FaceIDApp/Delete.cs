﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FaceIDApp
{
    public partial class Delete : Form
    {
        public Delete()
        {
            InitializeComponent();
        }

        private void BtnMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TxtCedula_Enter(object sender, EventArgs e)
        {
            if (TxtCedula.Text=="Cedula")
            {
                TxtCedula.Text = "";
            }
        }

        private void TxtCedula_Leave(object sender, EventArgs e)
        {
            if (TxtCedula.Text == "")
            {
                TxtCedula.Text = "Cedula";
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            DeleteUser();
        }

        void DeleteUser()
        {
            SqlConnection sqlConnection = new SqlConnection("server=DESKTOP-PC2MDLV; database=FaceAPP; integrated security=true");
            try
            {
                sqlConnection.Open();

                SqlCommand command = new SqlCommand($"DELETE FROM Usuarios WHERE Cedula=@ID;", sqlConnection);

                command.Parameters.AddWithValue("@ID", TxtCedula.Text);
                command.ExecuteNonQuery();

                MessageBox.Show("Los Datos Han Sido Actualizados Exitosamente", "Actualizados", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception error)
            {

                MessageBox.Show("Error, Por Favor Inténtelo Otra Vez", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //MessageBox.Show(error.Message);
            }
            finally
            {
                sqlConnection.Close();
            }
        }
    }
}

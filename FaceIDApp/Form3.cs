﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FaceIDApp
{
    public partial class Form3 : Form
    {
        SqlConnection sqlConnection = new SqlConnection("server=DESKTOP-PC2MDLV; database=FaceAPP; integrated security=true");
        public Form3()
        {
            InitializeComponent();
        }

        private void BtnGo_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu();
            this.Hide();
            menu.Show();
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BtnMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        DataTable Read()
        {
            SqlCommand command = new SqlCommand($"SELECT * from Usuarios", sqlConnection);

            SqlDataAdapter dataAdapter = new SqlDataAdapter
            {
                SelectCommand = command
            };
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);

            return dataTable;
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            DataGridView.DataSource = Read();
        }

        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            Update update = new Update();
            //this.Hide();
            update.ShowDialog();
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            Delete delete = new Delete();
            //this.Hide();
            delete.ShowDialog();
        }
    }
}

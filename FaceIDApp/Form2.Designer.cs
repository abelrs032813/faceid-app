﻿
namespace FaceIDApp
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.gunaElipse1 = new Guna.UI.WinForms.GunaElipse(this.components);
            this.Header = new System.Windows.Forms.Panel();
            this.BtnMin = new System.Windows.Forms.PictureBox();
            this.AppName = new System.Windows.Forms.Label();
            this.BtnCerrar = new System.Windows.Forms.PictureBox();
            this.gunaDragControl1 = new Guna.UI.WinForms.GunaDragControl(this.components);
            this.BtnGo = new Bunifu.Framework.UI.BunifuThinButton2();
            this.BtnBuscar = new Bunifu.Framework.UI.BunifuThinButton2();
            this.TxtCedula = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Foto = new Guna.UI.WinForms.GunaCirclePictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BtnMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Foto)).BeginInit();
            this.SuspendLayout();
            // 
            // gunaElipse1
            // 
            this.gunaElipse1.TargetControl = this;
            // 
            // Header
            // 
            this.Header.BackColor = System.Drawing.Color.Black;
            this.Header.Controls.Add(this.BtnMin);
            this.Header.Controls.Add(this.AppName);
            this.Header.Controls.Add(this.BtnCerrar);
            this.Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.Header.Location = new System.Drawing.Point(0, 0);
            this.Header.Name = "Header";
            this.Header.Size = new System.Drawing.Size(632, 41);
            this.Header.TabIndex = 4;
            // 
            // BtnMin
            // 
            this.BtnMin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnMin.Image = global::FaceIDApp.Properties.Resources.minus_solid;
            this.BtnMin.Location = new System.Drawing.Point(555, 10);
            this.BtnMin.Name = "BtnMin";
            this.BtnMin.Size = new System.Drawing.Size(31, 28);
            this.BtnMin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BtnMin.TabIndex = 2;
            this.BtnMin.TabStop = false;
            this.BtnMin.Click += new System.EventHandler(this.BtnMin_Click);
            // 
            // AppName
            // 
            this.AppName.AutoSize = true;
            this.AppName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AppName.ForeColor = System.Drawing.Color.White;
            this.AppName.Location = new System.Drawing.Point(30, 10);
            this.AppName.Name = "AppName";
            this.AppName.Size = new System.Drawing.Size(60, 19);
            this.AppName.TabIndex = 1;
            this.AppName.Text = "Buscar";
            // 
            // BtnCerrar
            // 
            this.BtnCerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnCerrar.Image = global::FaceIDApp.Properties.Resources.times_solid;
            this.BtnCerrar.Location = new System.Drawing.Point(592, 10);
            this.BtnCerrar.Name = "BtnCerrar";
            this.BtnCerrar.Size = new System.Drawing.Size(26, 28);
            this.BtnCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BtnCerrar.TabIndex = 0;
            this.BtnCerrar.TabStop = false;
            this.BtnCerrar.Click += new System.EventHandler(this.BtnCerrar_Click);
            // 
            // gunaDragControl1
            // 
            this.gunaDragControl1.TargetControl = this.Header;
            // 
            // BtnGo
            // 
            this.BtnGo.ActiveBorderThickness = 1;
            this.BtnGo.ActiveCornerRadius = 20;
            this.BtnGo.ActiveFillColor = System.Drawing.Color.SeaGreen;
            this.BtnGo.ActiveForecolor = System.Drawing.Color.White;
            this.BtnGo.ActiveLineColor = System.Drawing.Color.SeaGreen;
            this.BtnGo.BackColor = System.Drawing.SystemColors.Control;
            this.BtnGo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtnGo.BackgroundImage")));
            this.BtnGo.ButtonText = "Menú";
            this.BtnGo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnGo.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnGo.ForeColor = System.Drawing.Color.SeaGreen;
            this.BtnGo.IdleBorderThickness = 1;
            this.BtnGo.IdleCornerRadius = 20;
            this.BtnGo.IdleFillColor = System.Drawing.Color.White;
            this.BtnGo.IdleForecolor = System.Drawing.Color.SeaGreen;
            this.BtnGo.IdleLineColor = System.Drawing.Color.SeaGreen;
            this.BtnGo.Location = new System.Drawing.Point(405, 335);
            this.BtnGo.Margin = new System.Windows.Forms.Padding(5);
            this.BtnGo.Name = "BtnGo";
            this.BtnGo.Size = new System.Drawing.Size(181, 41);
            this.BtnGo.TabIndex = 10;
            this.BtnGo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.BtnGo.Click += new System.EventHandler(this.BtnGo_Click);
            // 
            // BtnBuscar
            // 
            this.BtnBuscar.ActiveBorderThickness = 1;
            this.BtnBuscar.ActiveCornerRadius = 20;
            this.BtnBuscar.ActiveFillColor = System.Drawing.Color.SeaGreen;
            this.BtnBuscar.ActiveForecolor = System.Drawing.Color.White;
            this.BtnBuscar.ActiveLineColor = System.Drawing.Color.SeaGreen;
            this.BtnBuscar.BackColor = System.Drawing.SystemColors.Control;
            this.BtnBuscar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtnBuscar.BackgroundImage")));
            this.BtnBuscar.ButtonText = "Buscar";
            this.BtnBuscar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnBuscar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBuscar.ForeColor = System.Drawing.Color.SeaGreen;
            this.BtnBuscar.IdleBorderThickness = 1;
            this.BtnBuscar.IdleCornerRadius = 20;
            this.BtnBuscar.IdleFillColor = System.Drawing.Color.White;
            this.BtnBuscar.IdleForecolor = System.Drawing.Color.SeaGreen;
            this.BtnBuscar.IdleLineColor = System.Drawing.Color.SeaGreen;
            this.BtnBuscar.Location = new System.Drawing.Point(49, 335);
            this.BtnBuscar.Margin = new System.Windows.Forms.Padding(5);
            this.BtnBuscar.Name = "BtnBuscar";
            this.BtnBuscar.Size = new System.Drawing.Size(181, 41);
            this.BtnBuscar.TabIndex = 11;
            this.BtnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.BtnBuscar.Click += new System.EventHandler(this.BtnBuscar_Click);
            // 
            // TxtCedula
            // 
            this.TxtCedula.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtCedula.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.TxtCedula.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.TxtCedula.HintForeColor = System.Drawing.Color.Empty;
            this.TxtCedula.HintText = "";
            this.TxtCedula.isPassword = false;
            this.TxtCedula.LineFocusedColor = System.Drawing.Color.SeaGreen;
            this.TxtCedula.LineIdleColor = System.Drawing.Color.Gray;
            this.TxtCedula.LineMouseHoverColor = System.Drawing.Color.SeaGreen;
            this.TxtCedula.LineThickness = 3;
            this.TxtCedula.Location = new System.Drawing.Point(16, 116);
            this.TxtCedula.Margin = new System.Windows.Forms.Padding(4);
            this.TxtCedula.Name = "TxtCedula";
            this.TxtCedula.Size = new System.Drawing.Size(249, 33);
            this.TxtCedula.TabIndex = 12;
            this.TxtCedula.Text = "Cedula";
            this.TxtCedula.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.TxtCedula.Enter += new System.EventHandler(this.TxtCedula_Enter);
            this.TxtCedula.Leave += new System.EventHandler(this.TxtCedula_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(67, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 19);
            this.label1.TabIndex = 3;
            this.label1.Text = "Datos Para Buscar";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(475, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 19);
            this.label2.TabIndex = 13;
            this.label2.Text = "Foto";
            this.label2.Visible = false;
            // 
            // Foto
            // 
            this.Foto.BaseColor = System.Drawing.Color.White;
            this.Foto.Location = new System.Drawing.Point(387, 106);
            this.Foto.Name = "Foto";
            this.Foto.Size = new System.Drawing.Size(217, 218);
            this.Foto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Foto.TabIndex = 14;
            this.Foto.TabStop = false;
            this.Foto.UseTransfarantBackground = false;
            this.Foto.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(15, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 19);
            this.label3.TabIndex = 15;
            this.label3.Text = "Nombre";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(15, 208);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 19);
            this.label4.TabIndex = 16;
            this.label4.Text = "Correo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(15, 304);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 19);
            this.label5.TabIndex = 17;
            this.label5.Text = "Fecha";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(15, 272);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 19);
            this.label6.TabIndex = 18;
            this.label6.Text = "Apartamneto";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(15, 240);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 19);
            this.label7.TabIndex = 19;
            this.label7.Text = "Edificio";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 386);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Foto);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtCedula);
            this.Controls.Add(this.BtnBuscar);
            this.Controls.Add(this.BtnGo);
            this.Controls.Add(this.Header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form2";
            this.Header.ResumeLayout(false);
            this.Header.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BtnMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Foto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Guna.UI.WinForms.GunaElipse gunaElipse1;
        private System.Windows.Forms.Panel Header;
        private System.Windows.Forms.PictureBox BtnMin;
        private System.Windows.Forms.Label AppName;
        private System.Windows.Forms.PictureBox BtnCerrar;
        private Guna.UI.WinForms.GunaDragControl gunaDragControl1;
        private Bunifu.Framework.UI.BunifuThinButton2 BtnGo;
        private Bunifu.Framework.UI.BunifuThinButton2 BtnBuscar;
        private Bunifu.Framework.UI.BunifuMaterialTextbox TxtCedula;
        private Guna.UI.WinForms.GunaCirclePictureBox Foto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
    }
}
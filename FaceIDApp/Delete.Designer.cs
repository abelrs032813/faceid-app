﻿
namespace FaceIDApp
{
    partial class Delete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Delete));
            this.gunaElipse1 = new Guna.UI.WinForms.GunaElipse(this.components);
            this.Header = new System.Windows.Forms.Panel();
            this.BtnMin = new System.Windows.Forms.PictureBox();
            this.AppName = new System.Windows.Forms.Label();
            this.BtnClose = new System.Windows.Forms.PictureBox();
            this.gunaDragControl1 = new Guna.UI.WinForms.GunaDragControl(this.components);
            this.TxtCedula = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.BtnDelete = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BtnMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnClose)).BeginInit();
            this.SuspendLayout();
            // 
            // gunaElipse1
            // 
            this.gunaElipse1.TargetControl = this;
            // 
            // Header
            // 
            this.Header.BackColor = System.Drawing.Color.Black;
            this.Header.Controls.Add(this.BtnMin);
            this.Header.Controls.Add(this.AppName);
            this.Header.Controls.Add(this.BtnClose);
            this.Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.Header.Location = new System.Drawing.Point(0, 0);
            this.Header.Name = "Header";
            this.Header.Size = new System.Drawing.Size(302, 41);
            this.Header.TabIndex = 2;
            // 
            // BtnMin
            // 
            this.BtnMin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnMin.Image = global::FaceIDApp.Properties.Resources.minus_solid;
            this.BtnMin.Location = new System.Drawing.Point(208, 10);
            this.BtnMin.Name = "BtnMin";
            this.BtnMin.Size = new System.Drawing.Size(31, 28);
            this.BtnMin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BtnMin.TabIndex = 2;
            this.BtnMin.TabStop = false;
            this.BtnMin.Click += new System.EventHandler(this.BtnMin_Click);
            // 
            // AppName
            // 
            this.AppName.AutoSize = true;
            this.AppName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AppName.ForeColor = System.Drawing.Color.White;
            this.AppName.Location = new System.Drawing.Point(30, 10);
            this.AppName.Name = "AppName";
            this.AppName.Size = new System.Drawing.Size(71, 19);
            this.AppName.TabIndex = 1;
            this.AppName.Text = "Eliminar";
            // 
            // BtnClose
            // 
            this.BtnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnClose.Image = global::FaceIDApp.Properties.Resources.times_solid;
            this.BtnClose.Location = new System.Drawing.Point(249, 10);
            this.BtnClose.Name = "BtnClose";
            this.BtnClose.Size = new System.Drawing.Size(26, 28);
            this.BtnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BtnClose.TabIndex = 0;
            this.BtnClose.TabStop = false;
            this.BtnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // gunaDragControl1
            // 
            this.gunaDragControl1.TargetControl = this.Header;
            // 
            // TxtCedula
            // 
            this.TxtCedula.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtCedula.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.TxtCedula.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.TxtCedula.HintForeColor = System.Drawing.Color.Empty;
            this.TxtCedula.HintText = "";
            this.TxtCedula.isPassword = false;
            this.TxtCedula.LineFocusedColor = System.Drawing.Color.SeaGreen;
            this.TxtCedula.LineIdleColor = System.Drawing.Color.Gray;
            this.TxtCedula.LineMouseHoverColor = System.Drawing.Color.SeaGreen;
            this.TxtCedula.LineThickness = 3;
            this.TxtCedula.Location = new System.Drawing.Point(26, 86);
            this.TxtCedula.Margin = new System.Windows.Forms.Padding(4);
            this.TxtCedula.Name = "TxtCedula";
            this.TxtCedula.Size = new System.Drawing.Size(249, 33);
            this.TxtCedula.TabIndex = 14;
            this.TxtCedula.Text = "Cedula";
            this.TxtCedula.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.TxtCedula.Enter += new System.EventHandler(this.TxtCedula_Enter);
            this.TxtCedula.Leave += new System.EventHandler(this.TxtCedula_Leave);
            // 
            // BtnDelete
            // 
            this.BtnDelete.ActiveBorderThickness = 1;
            this.BtnDelete.ActiveCornerRadius = 20;
            this.BtnDelete.ActiveFillColor = System.Drawing.Color.SeaGreen;
            this.BtnDelete.ActiveForecolor = System.Drawing.Color.White;
            this.BtnDelete.ActiveLineColor = System.Drawing.Color.SeaGreen;
            this.BtnDelete.BackColor = System.Drawing.SystemColors.Control;
            this.BtnDelete.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtnDelete.BackgroundImage")));
            this.BtnDelete.ButtonText = "Eliminar";
            this.BtnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnDelete.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.ForeColor = System.Drawing.Color.SeaGreen;
            this.BtnDelete.IdleBorderThickness = 1;
            this.BtnDelete.IdleCornerRadius = 20;
            this.BtnDelete.IdleFillColor = System.Drawing.Color.White;
            this.BtnDelete.IdleForecolor = System.Drawing.Color.SeaGreen;
            this.BtnDelete.IdleLineColor = System.Drawing.Color.SeaGreen;
            this.BtnDelete.Location = new System.Drawing.Point(58, 138);
            this.BtnDelete.Margin = new System.Windows.Forms.Padding(5);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(181, 41);
            this.BtnDelete.TabIndex = 13;
            this.BtnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // Delete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 212);
            this.Controls.Add(this.TxtCedula);
            this.Controls.Add(this.BtnDelete);
            this.Controls.Add(this.Header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Delete";
            this.Text = "Delete";
            this.Header.ResumeLayout(false);
            this.Header.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BtnMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnClose)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI.WinForms.GunaElipse gunaElipse1;
        private System.Windows.Forms.Panel Header;
        private System.Windows.Forms.PictureBox BtnMin;
        private System.Windows.Forms.Label AppName;
        private System.Windows.Forms.PictureBox BtnClose;
        private Guna.UI.WinForms.GunaDragControl gunaDragControl1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox TxtCedula;
        private Bunifu.Framework.UI.BunifuThinButton2 BtnDelete;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using FaceRecognition;

namespace FaceIDApp
{
    public partial class Form1 : Form
    {

        FaceRec faceRec = new FaceRec();

        //This APP WAS DEVELOP by Luis Fernando Méndez Aquino and Abel Jesus Rodriguez Solano
        public Form1()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            faceRec.openCamera(CameraImage, CaptureImage);
        }

        private void TxtName_Enter(object sender, EventArgs e)
        {
            if (TxtName.Text=="Nombre")
            {
                TxtName.Text = "";
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (TxtName.Text==""|| TxtCedula.Text == "" || TxtCorreo.Text == "" || TxtName.Text == "Nombre" || TxtCedula.Text == "Cedula" || TxtCorreo.Text == "Correo")
            {
                MessageBox.Show("Debe Completar todos los datos", "Verificar los Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                faceRec.Save_IMAGE(TxtName.Text);
                faceRec.isTrained = true;
                CaptureImage.Visible = true;
                Save();
            }
            
  
        }

        void Save()
        {
            SqlConnection sqlConnection = new SqlConnection("server=DESKTOP-PC2MDLV; database=FaceAPP; integrated security=true");
            try
            {

                sqlConnection.Open();
                SqlCommand command = new SqlCommand($"INSERT INTO Usuarios VALUES(@Name, @ID, @Email,@Edif, @Apt, @Date, @Pic);", sqlConnection);

                MemoryStream memoryStream = new MemoryStream();

                CaptureImage.Image.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                byte[] Pic = memoryStream.ToArray();

                command.Parameters.AddWithValue("@Pic", Pic);
                command.Parameters.AddWithValue("@Name", TxtName.Text);
                command.Parameters.AddWithValue("@ID", TxtCedula.Text);
                command.Parameters.AddWithValue("@Email", TxtCorreo.Text);
                command.Parameters.AddWithValue("@Edif", TxtEdif.Text);
                command.Parameters.AddWithValue("@Apt", TxtApt.Text);
                command.Parameters.AddWithValue("@Date", BirthDate.Value.ToString());

                command.ExecuteNonQuery();

                MessageBox.Show("Los Datos Han Sido Guardados Exitosamente", "Guardado" , MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception error)
            {
                MessageBox.Show("Error, Por Favor Inténtelo Otra Vez ", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show(error.Message);
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        private void BtnGo_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu();
            this.Hide();
            menu.Show();
        }

        private void BtnMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void TxtName_Leave(object sender, EventArgs e)
        {
            if (TxtName.Text == "")
            {
                TxtName.Text = "Nombre";
            }
        }

        private void TxtCedula_Enter(object sender, EventArgs e)
        {
            if (TxtCedula.Text == "Cedula")
            {
                TxtCedula.Text = "";
            }
        }

        private void TxtCedula_Leave(object sender, EventArgs e)
        {
            if (TxtCedula.Text == "")
            {
                TxtCedula.Text = "Cedula";
            }
        }

        private void TxtCorreo_Enter(object sender, EventArgs e)
        {
            if (TxtCorreo.Text == "Correo")
            {
                TxtCorreo.Text = "";
            }
        }

        private void TxtCorreo_Leave(object sender, EventArgs e)
        {
            if (TxtCorreo.Text == "")
            {
                TxtCorreo.Text = "Correo";
            }
        }

        private void TxtEdif_Enter(object sender, EventArgs e)
        {
            if (TxtEdif.Text == "Edificio")
            {
                TxtEdif.Text = "";
            }
        }

        private void TxtEdif_Leave(object sender, EventArgs e)
        {
            if (TxtEdif.Text == "")
            {
                TxtEdif.Text = "Edificio";
            }
        }

        private void TxtApt_Enter(object sender, EventArgs e)
        {
            if (TxtApt.Text == "Apartamento")
            {
                TxtApt.Text = "";
            }
        }

        private void TxtApt_Leave(object sender, EventArgs e)
        {
            if (TxtApt.Text == "")
            {
                TxtApt.Text = "Apartamento";
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FaceIDApp
{
    public partial class Update : Form
    {
        public Update()
        {
            InitializeComponent();
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            UpDate();
        }

        void UpDate()
        {
            SqlConnection sqlConnection = new SqlConnection("server=DESKTOP-PC2MDLV; database=FaceAPP; integrated security=true");
            try
            {
                sqlConnection.Open();
                              
                SqlCommand command = new SqlCommand($"UPDATE Usuarios SET Nombre=@Name, Correo=@Email, Edificio=@Edif, Apartemento=@Apt, FechaNac=@Date WHERE Cedula=@ID;", sqlConnection);

                command.Parameters.AddWithValue("@Name", TxtName.Text);
                command.Parameters.AddWithValue("@ID", TxtCedula.Text);
                command.Parameters.AddWithValue("@Email", TxtCorreo.Text);
                command.Parameters.AddWithValue("@Edif", TxtEdif.Text);
                command.Parameters.AddWithValue("@Apt", TxtApt.Text);
                command.Parameters.AddWithValue("@Date", BirthDate.Value.ToString());
                command.ExecuteNonQuery();

                MessageBox.Show("Los Datos Han Sido Actualizados Exitosamente", "Actualizados", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception error)
            {

                MessageBox.Show("Error, Por Favor Inténtelo Otra Vez", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //MessageBox.Show(error.Message);
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        private void TxtApt_Enter(object sender, EventArgs e)
        {
            if (TxtApt.Text == "Apartamento")
            {
                TxtApt.Text = "";
            }
        }

        private void TxtApt_Leave(object sender, EventArgs e)
        {
            if (TxtApt.Text == "")
            {
                TxtApt.Text = "Apartamento";
            }
        }

        private void TxtName_Enter(object sender, EventArgs e)
        {
            if (TxtName.Text=="Nombre")
            {
                TxtName.Text = "";
            }
        }

        private void TxtName_Leave(object sender, EventArgs e)
        {
            if (TxtName.Text == "")
            {
                TxtName.Text = "Nombre";
            }
        }

        private void TxtCedula_Enter(object sender, EventArgs e)
        {
            if (TxtCedula.Text == "Cedula")
            {
                TxtCedula.Text = "";
            }
        }

        private void TxtCedula_Leave(object sender, EventArgs e)
        {
            if (TxtCedula.Text == "")
            {
                TxtCedula.Text = "Cedula";
            }
        }

        private void TxtCorreo_Enter(object sender, EventArgs e)
        {
            if (TxtCorreo.Text == "Correo")
            {
                TxtCorreo.Text = "";
            }
        }

        private void TxtCorreo_Leave(object sender, EventArgs e)
        {
            if (TxtCorreo.Text == "")
            {
                TxtCorreo.Text = "Correo";
            }
        }

        private void TxtEdif_Enter(object sender, EventArgs e)
        {
            if (TxtEdif.Text == "Edificio")
            {
                TxtEdif.Text = "";
            }
        }

        private void TxtEdif_Leave(object sender, EventArgs e)
        {
            if (TxtEdif.Text == "")
            {
                TxtEdif.Text = "Edificio";
            }
        }
    }
}

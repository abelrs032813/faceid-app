﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FaceIDApp
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        void Buscar()
        {
            SqlConnection sqlConnection = new SqlConnection("server=DESKTOP-PC2MDLV; database=FaceAPP; integrated security=true");
            try
            {
                sqlConnection.Open();
                SqlCommand command = new SqlCommand($"Select * FROM Usuarios WHERE Cedula='{TxtCedula.Text}';", sqlConnection);

                SqlDataReader sqlDataReader = command.ExecuteReader();
                byte[] pic = new byte[0];

                if (sqlDataReader.Read())
                {
                    label3.Text = "Nombre: " + sqlDataReader["Nombre"].ToString();
                    label4.Text = "Correo: " + sqlDataReader["Correo"].ToString();
                    label7.Text = "Edificio: " + sqlDataReader["Edificio"].ToString();
                    label6.Text = "Apartamento: " + sqlDataReader["Apartemento"].ToString();

                    label5.Text = "Fecha de Nacimineto: " + sqlDataReader["FechaNac"].ToString();
                    pic = (byte[])sqlDataReader["Avatar"];

                    MemoryStream memoryStream = new MemoryStream(pic);

                    Foto.Image = Image.FromStream(memoryStream);

                    label2.Visible = true;
                    Foto.Visible = true;
                }
                MessageBox.Show("Se Ha Encontrado", "EXITO", MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch (Exception)
            {

                MessageBox.Show("Error, Por Favor Inténtelo Otra Vez", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        private void BtnMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void BtnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BtnGo_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu();
            this.Hide();
            menu.Show();
        }

        private void TxtCedula_Enter(object sender, EventArgs e)
        {
            if (TxtCedula.Text == "Cedula")
            {
                TxtCedula.Text = "";
            }
        }

        private void TxtCedula_Leave(object sender, EventArgs e)
        {
            if (TxtCedula.Text == "")
            {
                TxtCedula.Text = "Cedula";
            }
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            Buscar();
        }
    }
}
